## FH05 (500 points)

### Description

The attacker created a shared folder on the victims machine. Find this folder and give us the absolute path of the directory, including drive letter.

[files.allyourbases.co/fi02.zip](https://gitlab.com/bytepen/ctf/cyber-fasttrack-spring-2020/-/raw/master/challenge-files/fi02.zip)

### Solution

I managed to get this one on my own, but after the CTF ended, so I didn't get points.

If there's one tool that has really come out on top with this CTF, it's the latest version of Autopsy. As with a handful of challenges in the Forensics section, Autopsy made this challenge a breeze.

First, we need to figure out what we're looking for. It felt likely that shared drives would be stored in the registry. A quick Google search confirms that shared drives are stored in the following location:

```
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanServer\Shares
```

Now to get to the registry. We are given the memory-image.vmem file again, so let's use the following volatility command to extract the registry similar to how we extracted the process memory dump in [FH04](/forensics/fh04.md).

```
mkdir registry-output
volatility -f memory-image.vmem --profile=Win7SP1x64 dumpregistry -D registry-output
```

I clicked `New Case` assigned a Case Name and left the rest blank. When it was time to add a disk, I selected `Logical Files`, selected all the files in my `registry-output` folder, and clicked through the rest.

![](/images/fh05/1.png)

Once in Autopsy, I opened `Data Sources` => `LogicalFileSet1` => `registry.0x....SYSTEM.reg`

![](/images/fh05/2.png)

I then navigated to the following location:

```
HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\LanmanServer\Shares
```

Note that there is `ControlSet001` and `ControlSet002`. In this case, our target exists in `ControlSet001`. If we didn't find it there, I would have looked in `ControlSet002`. In this location, we have an `exfil` entry with a `Path` of **c:\recycle_bin**.

![](/images/fh05/3.png)

#### Answer: c:\recycle_bin

#### Tools Used

* Volatility
* Autopsy
