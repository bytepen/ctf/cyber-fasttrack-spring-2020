## Cyber FastTrack Spring 2020

This is a collection of write-ups and challenges from the Cyber FastTrack Spring 2020 Capture the Flag event. This even lasted from 19:00 25MAR2020 - 19:00 27MAR2020 CDT.

I didn't complete all of the challenges, so there will be some gaps in the solutions until I either figure them out or someone helps me along. It also occurred to me that I wanted to do write-ups after the event ended, so I may be missing some exact details and screenshots that would be nice. The event coordinators allow us to share the challenges and they intend to release them all in the near future, so hopefully I can go back and clear some of those up.

In the meantime, Tsuto (jselliott) published his write-ups of the challenges as well. If you'd like a very quick summary of how to complete each challenge, I would encourage you to check it out [here](https://github.com/jselliott/CyberFastTrack_SP2020). I will be going into more detail with my write-ups, but also plan to keep them concise. Hopefully you will find them worth a read!

#### Links

Note that all of the links to files and challenges went down when the event ended. If you click on a link, it is most likely downloading it from my uploads [here](/challenge-files.md). If you find any links aren't working, please let me know by submitting a [GitLab Issue](https://gitlab.com/bytepen/ctf/cyber-fasttrack-spring-2020/-/issues).

#### Challenges

In total, there were 43 challenges, worth 14,670 points as part of the Capture the Flag Event. These challenges were broken into the following 7 categories:

* [General](/general.md)
* [Forensics](/forensics.md)
* [WebApp](/webapp.md)
* [Reverse Engineering](/reverse-engineering.md)
* [Binary Exploitation](/binary-exploitation.md)
* [Network](/network.md)

#### Ending Leaderboard

| Position | Name | % | Points |
|--|--|--|--|
| 1st | Tsuto | 95% | 13,650 |
| 2nd | bdubya | 91% | 12,900 |
| 3rd | bryson | 91% | 12,650 |
| 4th | enio | 88% | 12,150 |
| 5th | H3xCat | 81% | 11,150 |
| 6th | Sadican | 84% | 10,900 |
| 6th | 0x3f3 | 84% | 10,900 |
| 6th | 0xF4T1H | 84% | 10,900 |
| 6th | oxm4m1 | 84% | 10,900 |
| 10th | Nyxodile | 81% | 10,650 |
| ... | ... | ... | ... |
| 267th | BytePen | 40%<br>(17/43) | 3,250 |
