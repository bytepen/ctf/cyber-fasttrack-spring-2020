## TE02 (100 points)

### Description 

What was the name of the first computer virus who's message was 'catch me if you can'?

### Solution

This challenge also just required a small amount of [Googling](https://www.google.com/search?q=catch+me+if+you+can+virus) which led me to the following article on Science Direct

[Catch Me if You Can](https://www.sciencedirect.com/topics/computer-science/catch-me-if-you-can)

![](/images/te02/1.png)

Here we can see that the virus identified as the **creeper**.

#### Answer: creeper

#### Tools Used

* Google
