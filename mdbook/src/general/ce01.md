## CE01 (100 points)

### Description 

We have intercepted this rather strange looking message, So we want you to take a look and see if you can work its meaning.

Flag is all UPPER CASE.

[https://files.allyourbases.co/ce01.zip](https://gitlab.com/bytepen/ctf/cyber-fasttrack-spring-2020/-/raw/master/challenge-files/ce01.zip)

### Solution

I found this one pretty interesting because I went about it the completely wrong way, but I would have still been able to get the answer given enough time. In this challenge, we were given the following image and asked to get meaning from it:

![](/images/ce01/1.png)

In jselliot's solution (found [here](https://github.com/jselliott/CyberFastTrack_SP2020/tree/master/Challenges/CE01)), he pointed out that this is apparently a [pigpen cipher](https://en.wikipedia.org/wiki/Pigpen_cipher), which is a pretty well known cipher that looks like this:

![](/images/ce01/2.png)

All you have to do is match up the shapes to the letters and you'll get the message **bacon_secret_codes**.

Now for my, way over the top solution. I had no idea what this was, so I decided to take a crack at bruteforcing it. I made the assumption that each unique image was a unique character and the underscore separated words. I mapped out the characters to numbers, imported a list of [5 letter words](https://github.com/charlesreid1/five-letter-words) and [6 letter words](https://github.com/exec/umac) I found on GitHub, and create some code to try and randomly guess the password because I wasn't sure how to do it in order. This version of the code was as follows:

```
import random

mapping = {}

with open("5-letter-words.txt") as f:
    five_letter_words = f.read().splitlines()

with open("6-letter-words.txt") as f:
    six_letter_words = f.read().splitlines()

while True:
    letters = list("abcdefghijklmnopqrstuvwxyz")
    encrypted = ["12345", "673879", "34076"]
    decrypted = []
    for i in range(0,10):
        j = random.randint(0, len(letters) - 1)
        mapping[str(i)] = letters[j]
        letters.pop(j)

    for word in encrypted:
        for key in mapping.keys():
            word = word.replace(key, mapping[key])
        decrypted.append(word)

    if decrypted[0] in five_letter_words and decrypted[1] in six_letter_words and decrypted[2] in five_letter_words:
        print("_".join(decrypted).upper())
```

I ran this for a few hours, but didn't have much luck. I moved on until I saw jselliot's solution and saw that I was right in some of my assumptions and the words were in the wordlists I had chosen. This means that, given enough time, this solution would have worked. I ran several instances of the script overnight with no luck. Determined to do it the hard way, I decided to improve on my script and found out about python's itertools and decided to take a more structured approach with the following script:

```
#!/usr/bin/python3

# 0: U
# 1: Backward L
# 2: L
# 3: C with dot
# 4: O with dot
# 5: V
# 6: O
# 7: F dot
# 8: >
# 9: Backward C

import string
import itertools
from datetime import datetime

class Decrypter():
    def __init__(self):
        print(f"Started: {datetime.now()}\n\n")
        self.five = self.read_file("5-letter-words.txt")
        self.six = self.read_file("6-letter-words.txt")
        self.enc = ["01234", "562768", "23965"]
        for code in itertools.permutations(string.ascii_lowercase, r=10):
            self.dec = []
            self.check_code(code)

    @staticmethod
    def read_file(loc):
        with open(loc) as f:
            return f.read().splitlines()

    def check_code(self, code):
        for enc in self.enc:
            for i in range(0,10):
                enc = enc.replace(str(i), code[i])
            self.dec.append(enc)

        if self.dec[0] in self.five and self.dec[1] in self.six and self.dec[2] in self.five:
            print(" ".join(self.dec))
            print(f"Found: {datetime.now()}\n\n")

if __name__ == "__main__":
    d = Decrypter()
```

I'm currently running this and when it outputs the flag, I'll be sure to update this article with stats. In the meantime, just do it the easy way. XD


Update: After running in my VM for 73 hours, I'm going to go ahead and call it quits. It occurred to me after the fact that I should have probably put in a way to check the current iteration, but I didn't. Either way, with my current setup, it would have taken longer to brute force the challenge than the challenge lasted. Oh well! That was fun.

#### Answer: bacon_secret_codes

#### Tools Used

* PigPen Cipher Image (Easy Mode)
* Python (Hard Mode)
